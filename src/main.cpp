﻿// common_math_tests.cpp : Defines the entry point for the application.
//

#include "common_math_tests.h"
#include "gtest/gtest.h"

using namespace std;

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();
    return ret;
}