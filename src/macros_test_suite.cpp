#define _USE_MATH_HELPERS_

#include "gtest/gtest.h"
#include "common_math.h"

TEST(GMacrosTestSuite, CubeTest1)
{
    int n = 10;
    for (int i = 1; i <= n; ++i)
    {
        int expected = (i + 1) * (i + 1) * (i + 1);
        int actual = CUBE(i + 1);
        EXPECT_EQ(expected, actual);
    }
}

TEST(GMacrosTestSuite, CubeTest2)
{
    double a = -10, b = 10, h = 0.5;
    for (double x = a; x <= b; x += h) {
        double shift = 0.3423552;
        double expected = (x + shift) * (x + shift) * (x + shift);
        double actual = CUBE(x + shift);
        ASSERT_DOUBLE_EQ(expected, actual);
    }
}

TEST(GMacrosTestSuite, NRootTestODD1)
{
    double a = -10, b = 10, h = 0.5;
    for (double x = a; x <= b; x += h)
    {
        double expected = x < 0 ? -pow(abs(x), 1.0 / 3) : pow(x, 1.0 / 3);
        double actual = N_ROOT(x, 3);
        ASSERT_DOUBLE_EQ(expected, actual);
    }
}