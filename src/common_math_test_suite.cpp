#include "gtest/gtest.h"

#define _USE_MATH_HELPERS_

#include "common_math.h"

TEST(GCommonTestSuite, GeneratedNumbersShouldBeInTheRange)
{
    int n = 20, offset = 5;
    for (int i = 1; i <= n; i += offset)
    {
        int actual = common::math::generate_random(i, i + offset);
        EXPECT_TRUE((actual >= i && actual < i + offset));
    }
}